<?php

namespace Ls\AllegroBundle\Service;

use Knp\Menu\ItemInterface;
use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminLink;
use Ls\CoreBundle\Helper\AdminRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminService {
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function addToMenu(ItemInterface $parent, $route, $set) {
        $item = $parent->addChild('Lista ofert', array(
            'route' => 'ls_admin_allegro',
        ));
        
        $current_set = true;

        if (!$set) {
            switch ($route) {
                case 'ls_admin_allegro':
                    $item->setCurrent(true);
                    break;
                default:
                    $current_set = false;
                    break;
            }
        }
        
        $item = $parent->addChild('Kategorie Allegro', array(
            'route' => 'ls_admin_allegro_categories',
        ));
        
        $current_set = true;

        if (!$set) {
            switch ($route) {
                case 'ls_admin_allegro_categories':
                    $item->setCurrent(true);
                    break;
                default:
                    $current_set = false;
                    break;
            }
        }
        
        $item = $parent->addChild('Szablony filtrów', array(
            'route' => 'ls_admin_allegro_templates',
        ));

        $current_set = true;

        if (!$set) {
            switch ($route) {
                case 'ls_admin_allegro_templates':
                    $item->setCurrent(true);
                    break;
                default:
                    $current_set = false;
                    break;
            }
        }

        return $current_set;
    }

    public function addToDashboard(AdminBlock $parent) {
        $router = $this->container->get('router');

        $row = new AdminRow('Oferty');
        $parent->addRow($row);

        $row->addLink(new AdminLink('Lista', 'glyphicon-list', $router->generate('ls_admin_allegro')));

        $row = new AdminRow('Kategorie Allegro');
        $parent->addRow($row);
                
        $row->addLink(new AdminLink('Kategorie Allegro', 'glyphicon-eye-open', $router->generate('ls_admin_allegro_categories')));
        
        $row = new AdminRow('Szablony wyszukiwania');
        $parent->addRow($row);
        
        $row->addLink(new AdminLink('Dodaj', 'glyphicon-plus', $router->generate('ls_admin_allegro_addfilter_template')));
        $row->addLink(new AdminLink('Zarządzaj', 'glyphicon-list', $router->generate('ls_admin_allegro_templates')));
    }
}

