<?php

namespace Ls\AllegroBundle\Form;

use Symfony\Component\Form\AbstractType;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilterTemplateType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        $builder->add('title', null, array(
            'label' => 'Nazwa',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        
        $builder->add('category', EntityType::class, array(
            'label' => 'Wybierz kategorię',
            'class' => 'LsAllegroBundle:Category',
            'multiple' => false,
            'required' => true,
            'choice_label' => 'itemTitle',
            'attr' => ['style' => "display:none;"],
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('m')
                          ->orderBy('m.itemTitle', 'ASC');
            },
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\AllegroBundle\Entity\FilterTemplate',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_allegro_template';
    }
}
