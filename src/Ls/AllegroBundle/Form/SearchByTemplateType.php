<?php

namespace Ls\AllegroBundle\Form;

use Ls\CoreBundle\Form\DataTransformer\DateTimeTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;

use Ls\AllegroBundle\Entity\FilterTemplate;

class SearchByTemplateType extends AbstractType {
    
    private $filterId = 0;
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        $builder->add('itemTitle', EntityType::class, array(
            'placeholder' => ' -- brak szablonu --',
            'class' => 'LsAllegroBundle:FilterTemplate',
            'choice_value' => 'itemTitle',
            'required' => false,
            'label' => 'Szablon wyszukiwania:',
            'expanded' => false,
            'multiple' => false
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\AllegroBundle\Entity\FilterTemplate',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_allegro_template_search';
    }
}
