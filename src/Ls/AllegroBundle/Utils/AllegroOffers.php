<?php

namespace Ls\AllegroBundle\Utils;

use Zoondo\AllegroApi\AllegroApi;
use Ls\AllegroBundle\Entity\Category;
use Ls\AllegroBundle\Entity\FilterTemplate;
use Doctrine\ORM\EntityManager;
use Ls\AllegroBundle\Utils\OffersModel;
use SoapClient;

class AllegroOffers {
    private $key = '263f5594';
    private $em;
    private $category;
    private $template;
    private $yearFilter = null;
    private $wsdlUrl = "https://webapi.allegro.pl/service.php?wsdl";
    private $allegroApi = null;
    private $byYears = true;
    
    public function __construct(EntityManager $em, FilterTemplate $template, Category $category) {
        $this->category = $category;
        $this->template = $template;
        $this->byYears = $template->getByYears();
        $this->em = $em;
        $this->allegroApi = new AllegroAPI($this->key);
        $client = new SoapClient($this->wsdlUrl ,array(
            'trace' =>true,
            'connection_timeout' => 100000,
            'keep_alive' => false,
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS
        ));
        $this->allegroApi->setSoapClient($client);
        
        foreach ($this->category->getFilters() as $filter) {
            if ($filter->getTitle() == 'Rok produkcji') {
                $this->yearFilter = $filter;
            }
        }
        
        $this->settings = $this->prepareSettings($template, $category);
    }
    
    private function prepareSettings(FilterTemplate $template, Category $category) {
        $settings['webapiKey'] = '263f5594';
        $settings['countryId'] = 1;
        $settings['sortOptions'] = ['sortType' => 'priceDelivery', 'sortOrder' => 'asc'];
        $settings['resultSize'] = 100;
        
        $filters = $this->prepareFilters($template);
        $filters[] = ['filterId' => 'category', 'filterValueId' => [$category->getCategoryAllegroId()]];
        
        $settings['filterOptions'] = $filters;
        
        return $settings;
    }
    
    private function prepareFilters(FilterTemplate $template) {
        $filterOptions = [];
        
        $em = $this->em;
        $templateFilters = json_decode($template->getFilters(), true);
        
        foreach ($templateFilters as $filter => $values) {
            if (!empty($filter) && !empty($values)) {
                $filterOption = [];

                if (preg_match("/start_(.*)/", $filter, $matches)) {
                    $filterEntity = $em->createQueryBuilder()
                        ->select('f')
                        ->from('LsAllegroBundle:Filter', 'f')
                        ->leftJoin('f.categories', 'c')
                        ->where('f.id = :filterId')
                        ->andWhere('c.id = :categoryId')
                        ->setParameter('filterId', $matches[1])
                        ->setParameter('categoryId', $this->category->getId())
                        ->getQuery()
                        ->getOneOrNullResult();
                    if ($filterEntity) {
                        if ($filterEntity != $this->yearFilter) { //exclude year filter - year filter in getOffersByYear
                            $filterOption['filterId'] = $filterEntity->getFilterId();
                            $filterOption['filterValueRange']['rangeValueMin'] = $values;
                            $filterOptions[$matches[0]] = $filterOption;
                        }
                    }

                } elseif (preg_match("/end_(.*)/", $filter, $matches)) {
                    $filterEntity = $em->createQueryBuilder()
                        ->select('f')
                        ->from('LsAllegroBundle:Filter', 'f')
                        ->leftJoin('f.categories', 'c')
                        ->where('f.id = :filterId')
                        ->andWhere('c.id = :categoryId')
                        ->setParameter('filterId', $matches[1])
                        ->setParameter('categoryId', $this->category->getId())
                        ->getQuery()
                        ->getOneOrNullResult();

                    if ($filterEntity) {
                        if (key_exists('start_'.$matches[1], $filterOptions)) {
                            $filterOptions['start_'.$matches[1]]['filterValueRange']['rangeValueMax'] = $values;
                        } else {
                            $filterOption['filterId'] = $filterEntity->getFilterId();
                            $filterOption['filterValueRange']['rangeValueMax'] = $values;
                            $filterOptions[] = $filterOption;
                        }
                    }
                } else {
                    $filterEntity = $em->createQueryBuilder()
                        ->select('f')
                        ->from('LsAllegroBundle:Filter', 'f')
                        ->leftJoin('f.categories', 'c')
                        ->where('f.id = :filterId')
                        ->andWhere('c.id = :categoryId')
                        ->setParameter('filterId', $filter)
                        ->setParameter('categoryId', $this->category->getId())
                        ->getQuery()
                        ->getOneOrNullResult();
                    
                    if ($filterEntity) {
                        $filterOption['filterId'] = $filterEntity->getFilterId();

                        if (is_array($values)) {
                            $filterValues = $filterEntity->getValues();
                            foreach ($filterValues as $filterValue) {
                                if (in_array($filterValue->getId(), $values)) {
                                    $filterOption['filterValueId'][] = $filterValue->getFilterValueId();
                                }
                            }
                        } else {
                            $filterOption['filterValueId'][] = $values;
                        } 
                        $filterOptions[] = $filterOption;
                    }
                }
            }
        }
        
        return array_values($filterOptions);
    }
    
    public function getOffers() {
        ini_set('memory_limit', '4G');
        set_time_limit(100000);
        ini_set("default_socket_timeout", 100000);
        
        $offers = new OffersModel();
        
        if ($this->yearFilter != null && $this->byYears) {
            $offers->setOffers($this->getOffersByYear());
            $offers->setHasYear(true);
        } else {
            $allegroItemsList = $this->allegroApi->doGetItemsList($this->settings);
            if ($allegroItemsList->itemsCount > 0) {
                $offers->setOffers([$allegroItemsList->itemsList->item]);
            }
            $offers->setHasYear(false);
        }
        
        $offers = $this->prepareOffers($offers); //skip the promoted offers at the beginning of the list

        return $offers;
    }
    
    public function getOffersByYear() {
        $filters = json_decode($this->template->getFilters(), true);
        $startYear = 1980;
        $endYear = idate('Y');
        $offers = [];
        
        if ($this->yearFilter != null) {
            if (key_exists("start_".$this->yearFilter->getId(), $filters)) {
                if (!empty($filters["start_".$this->yearFilter->getId()])) {
                    $startYear = $filters["start_".$this->yearFilter->getId()];
                }
            }
            if (key_exists("end_".$this->yearFilter->getId(), $filters)) {
                if (!empty($filters["end_".$this->yearFilter->getId()])) {
                    $endYear = $filters["end_".$this->yearFilter->getId()];
                }
            }
            
            $time1 = microtime(true);
            //get offer from each year
            for ($checkYear = $startYear; $checkYear <= $endYear; $checkYear++) {
                $currentSetting = $this->settings;
                
                $filterOption = [];
                
                $filterOption['filterId'] = $this->yearFilter->getFilterId();
                $filterOption['filterValueRange']['rangeValueMin'] = $checkYear;
                $filterOption['filterValueRange']['rangeValueMax'] = $checkYear;
                
                $currentSetting['filterOptions'][] = $filterOption;
                
                $allegroItemsList = $this->allegroApi->doGetItemsList($currentSetting);
                
                sleep(1); //because allegro has limit of connections per second
                
                if ($allegroItemsList->itemsCount > 0) {
                    $offers[$checkYear] = $allegroItemsList->itemsList->item;
                }
            }
            $time2 = microtime(true);
        }
        
        return $offers;
    }
    
    //skip the promoted offers at the beginning of the list
    private function prepareOffers($offers) {
        $limit = ($this->template->getResultSize() > 0) ? $this->template->getResultSize() : 30;
        $preparedOffers = [];
        
        foreach ($offers->getOffers() as $year => $offersList) {
            usort($offersList, array($this, "sortByPrice"));
            $offersList = array_slice($offersList, 0, $limit);
            $preparedOffers[$year] = $offersList;
        }
        
        $offers->setOffers($preparedOffers);
        
        return $offers;
    }
    
    private function sortByPrice($a, $b)
    {
        return strcmp($a->priceInfo->item[0]->priceValue, $b->priceInfo->item[0]->priceValue);
    }
}
