<?php
    
namespace Ls\AllegroBundle\Utils;

use FeedIo\Feed;
use FeedIo\Feed\Item;
use FeedIo\FeedInterface;
use Ls\AllegroBundle\Entity\FilterTemplate;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

class OffersRssProvider {

    /**
     * default provider.
     */
    const DEFAULT_SOURCE = 'debril.provider.default';
    /**
     * parameter used to force refresh at every hit (skips 'If-Modified-Since' usage).
     * set it to true for debug purpose.
     */
    const FORCE_PARAM_NAME = 'force_refresh';
    /**
     * @var \DateTime
     */
    protected $since;
    
    private $container;
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }
    
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function getRss(Request $request, FilterTemplate $template = null, $offers, $lastModified)
    {
        $options = $request->attributes->get('_route_params');
        $this->setModifiedSince($request);
        $options['Since'] = $this->getModifiedSince();
        
        return $this->createStreamResponse(
            $options,
            $request->get('format', 'rss'),
            $template, 
            $offers,
            $lastModified
        );
    }
    
    /*
     * options = [template entity, offers]
     */
    public function getFeedContent(FilterTemplate $template = null, $offers, $lastModified)
    {
        $feed = new Feed();
        if ($template) {
            $feed->setTitle($template->getTitle());
        } else {
            $feed->setTitle("Wszystkie oferty okazjonalne");
        }
        $feed->setLastModified($lastModified);
        
        foreach($this->getItems($offers) as $item ) {
            $feed->add($item);
        }

        return $feed;
    }

    protected function getItems($offers)
    {
        foreach($offers as $offer) {
            $item = new Item;
            $item->setTitle($offer->getTitle());
            
            $description = "Cena: ".$offer->getPrice()." PLN. ";
            if ($offer->getYear() >= 1900) {
                $description .= "Rok produkcji: ".$offer->getYear().". ";
            }
            
            $categoryDescription = "Kategoria: ";
            foreach ($offer->getCategory()->getAllParents() as $parentCategory) {
                $categoryDescription .= $parentCategory->getItemTitle() . " -> ";
            }
            $categoryDescription .= $offer->getCategory()->getItemTitle();
            $description .= $categoryDescription;
            
            $item->setDescription($description);
            $item->setLastModified($offer->getCreatedAt());
            
            $link = "http://allegro.pl/show_item.php?item=".$offer->getItemId();
            $item->setLink($link);
            
            yield $item;
        }
    }
    protected function fetchFromStorage()
    {
        // query the database to fetch items
    }
    
    protected function createStreamResponse(array $options, $format, FilterTemplate $template = null, $offers, $lastModified, $source = self::DEFAULT_SOURCE)
    {
        $content = $this->getFeedContent($template, $offers, $lastModified);
        if ($this->mustForceRefresh() || $content->getLastModified() > $this->getModifiedSince()) {
            $response = new Response($this->getFeedIo()->format($content, $format));
            $response->headers->set('Content-Type', 'application/xhtml+xml');
            $this->setFeedHeaders($response, $content);
        } else {
            $response = new Response();
            $response->setNotModified();
        }
        return $response;
    }
    
    /**
     * @param Response $response
     * @param FeedInterface $feed
     * @return $this
     */
    protected function setFeedHeaders(Response $response, FeedInterface $feed)
    {
        $response->headers->set('Content-Type', 'application/xhtml+xml');
        if (! $this->isPrivate() ) {
            $response->setPublic();
        }
        $response->setMaxAge(3600);
        $response->setLastModified($feed->getLastModified());
        return $this;
    }
    
    /**
     * Returns true if the controller must ignore the last modified date.
     *
     * @return bool
     */
    protected function mustForceRefresh()
    {
        if ($this->container->hasParameter(self::FORCE_PARAM_NAME)) {
            return $this->container->getParameter(self::FORCE_PARAM_NAME);
        }
        return false;
    }
    
    /**
     * @param Request $request
     *
     * @return $this
     */
    protected function setModifiedSince(Request $request)
    {
        $this->since = new \DateTime();
        if ($request->headers->has('If-Modified-Since')) {
            $string = $request->headers->get('If-Modified-Since');
            $this->since = \DateTime::createFromFormat(\DateTime::RSS, $string);
        } else {
            $this->since->setTimestamp(1);
        }
        return $this;
    }
    
    /**
     * Extract the 'If-Modified-Since' value from the headers.
     *
     * @return \DateTime
     */
    protected function getModifiedSince()
    {
        if (is_null($this->since)) {
            $this->since = new \DateTime('@0');
        }
        return $this->since;
    }
    
    /**
     * @return \FeedIo\FeedIo
     */
    protected function getFeedIo()
    {
        return $this->container->get('feedio');
    }
    
    /**
     * @return boolean true if the feed must be private
     */
    protected function isPrivate()
    {
        return $this->container->getParameter('debril_rss_atom.private_feeds');
    }
}
