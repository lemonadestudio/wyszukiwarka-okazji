<?php

namespace Ls\AllegroBundle\Utils;

class OffersModel {
    
    private $hasYears = false;
    private $offers = [];
    private $offersAllegroIds = [];
    
    public function setHasYear($hasYears) {
        $this->hasYears = $hasYears;
        
        return $this;
    }
    
    public function getHasYear() {
        return $this->hasYears;
    }
    
    public function setOffers($offers = []) {
        $this->offers = $offers;
        
        $this->setOffersAllegroIds();
        
        return $this;
    }
    
    public function getOffers() {
        return $this->offers;
    }
    
    public function addOfferAllegroId($itemId) {
        if (!in_array($itemId, $this->offersAllegroIds)) {
            $this->offersAllegroIds[] = $itemId;
        }
            
        return $this;
    }
    
    public function setOffersAllegroIds() {
        $offersAllegroIds = [];
        
        foreach ($this->offers as $year => $offersList) {
            foreach ($offersList as $offer) {
                $offersAllegroIds[] = $offer->itemId;
            }
        }
        
        
        $this->offersAllegroIds = $offersAllegroIds;
        
        return $this;
    }
    
    public function getOffersAllegroIds() {
        $this->setOffersAllegroIds();
        
        return $this->offersAllegroIds;
    }
   
}
