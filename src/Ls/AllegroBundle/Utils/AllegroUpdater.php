<?php

namespace Ls\AllegroBundle\Utils;

use Zoondo\AllegroApi\AllegroApi;
use SoapClient;

/**
 * Class responsible for updating auction from allegro and related stuff.
 * Result size of 100 is maximum allowed by Allegro amount of results.
 * @key is Allegro WebAPI key, generate on Allegro's account.
 */
class AllegroUpdater {
    private $key = '263f5594';
    private $settings = array(
        'webapiKey' => '263f5594',
        'countryId' => 1,
        'filterOptions' => array(),
        'resultSize' => 100,
        'sortOptions' => array(
            array(
                'sortType' => 'endingTime',
                'sortOrder' => 'desc'
            )
        )
    );
    private $wsdlUrl = "https://webapi.allegro.pl/service.php?wsdl";
    private $allegroApi = null;
    
    public function __construct() {
        $this->allegroApi = new AllegroAPI($this->key);
        $client = new SoapClient($this->wsdlUrl ,array(
            'trace' =>true,
            'connection_timeout' => 100000,
            'keep_alive' => false,
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS
        ));
        $this->allegroApi->setSoapClient($client);
    }
    
    /**
     * Checks if list of Filters is up to date.
     * @return array
     */
    public function updateFiltersList($categoryId) {
        set_time_limit(100000);
        ini_set("default_socket_timeout", 100000);
        
        $settings = $this->settings;
        $settings['filterOptions'] = array(array('filterId' => 'category', 'filterValueId' => array($categoryId)));
        $settings['resultSize'] = 1000;
        
        $filters = $this->allegroApi->doGetItemsList($settings)->filtersList->item;
        sleep(1); //because allegro has limit of connections per second
        
        return $filters;
        
    }
        
    /**
     * Returns data of all categories.
     * @return array
     */
    public function getCategoryTree() {
        $categories = [];
        
        $categoriesTree = $this->allegroApi->doGetCatsData(array(
            'countryId' => 1,
            'webapiKey' => $this->key
        ));
        
        return $categoriesTree->catsList->item;
    }
    
    public function getOfferItemsInfo($itemsIdArray) {
        $user_login = "oleh@lemonadestudio.pl";
        $user_password = "Allegrowebapi123"; 
        $url = "http://webapi.allegro.pl/uploader.php?wsdl";
        
        $client = new SoapClient($url ,array(
            'trace' =>true,
            'connection_timeout' => 100000,
            'keep_alive' => false,
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS
        ));
        
        $varQuery = $client->doQuerySysStatus(1, 1, $this->key);
        $allegroVer = $varQuery['ver-key'];
        $local_version = $allegroVer; 
        
        $varSession = $client->doLogin($user_login, $user_password, 1, $this->key, $local_version);

        $itemsInfo = $client->doGetItemsInfo($varSession['session-handle-part'], $itemsIdArray, 1, 0, 0, 0, 0, 0, 0);
        
        sleep(1); //because allegro has limit of connections per second
        
        return $itemsInfo['array-item-list-info'];
    }
}
