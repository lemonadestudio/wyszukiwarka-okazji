<?php

namespace Ls\AllegroBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CoreBundle\Utils\Tools;
use Ls\AllegroBundle\Entity\Allegro;

class AllegroUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'postPersist',
            'preUpdate',
            'postUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Allegro) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
            
        }
    }

    public function postPersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Allegro) {
            if (strcmp($entity->getOldSlug(), $entity->getSlug()) !== 0) {
                if (null !== $entity->getOldSlug()) {
                    $query = $em->createQueryBuilder()
                        ->select('c')
                        ->from('LsMenuBundle:MenuItem', 'c')
                        ->where('c.route LIKE :route')
                        ->andWhere('c.route_parameters LIKE :slug')
                        ->setParameter('route', 'ls_news_show')
                        ->setParameter('slug', '%' . $entity->getOldSlug() . '%')
                        ->getQuery();

                    $items = $query->getResult();

                    foreach ($items as $item) {
                        $item->setRouteParameters('{"slug":"' . $entity->getSlug() . '"}');
                        $em->persist($item);
                    }
                    $em->flush();

                    $entity->setOldSlug($entity->getSlug());
                    $em->persist($entity);
                    $em->flush();
                } else {
                    $entity->setOldSlug($entity->getSlug());
                    $em->persist($entity);
                    $em->flush();
                }
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Allegro) {
            $entity->setCreatedAt(new \DateTime());
            
        }
    }

    public function postUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Allegro) {
            if (strcmp($entity->getOldSlug(), $entity->getSlug()) !== 0) {
                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsMenuBundle:MenuItem', 'c')
                    ->where('c.route LIKE :route')
                    ->andWhere('c.route_parameters LIKE :slug')
                    ->setParameter('route', 'ls_news_show')
                    ->setParameter('slug', '%' . $entity->getOldSlug() . '%')
                    ->getQuery();

                $items = $query->getResult();

                foreach ($items as $item) {
                    $item->setRouteParameters('{"slug":"' . $entity->getSlug() . '"}');
                    $em->persist($item);
                }
                $em->flush();

                $entity->setOldSlug($entity->getSlug());
                $em->persist($entity);
                $em->flush();
            }
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Allegro) {
            $entity->deletePhoto();
        }
    }

}