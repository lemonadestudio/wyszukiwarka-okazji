<?php

namespace Ls\AllegroBundle\Controller;

use Ls\AllegroBundle\Entity\FilterTemplate;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Ls\AllegroBundle\Entity\FilterValue;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class AdminTemplateController extends Controller {
    private $pager_limit_name = 'admin_allegro_pager_limit';
    
    /**
     * Responsible for list of templates of filters.
     * @return object
     */
    public function templatesAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');
        
        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }
        $query = $em->createQueryBuilder()
            ->select('t')
            ->from('LsAllegroBundle:FilterTemplate', 't')
            ->getQuery()
            ->getResult();
        
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'created_at',
                'defaultSortDirection' => 'desc',
            )
        );
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');
        
        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_allegro'));
        }
                
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Szablony filtrów', $this->get('router')->generate('ls_admin_allegro_templates'));
        
        return $this->render('LsAllegroBundle:AdminTemplate:index.html.twig', array(
            'entities' => $entities,
            'page' => $page,
            'limit' => $limit
        ));
        
    }
    
    public function getJsonCategoriesTreeAction($categoryId = null) {
        $em = $this->getDoctrine()->getManager();
        $categoriesArray = [];
        
        $parentCategories = $query = $em->createQueryBuilder()
            ->select('c')
            ->from('LsAllegroBundle:Category', 'c')
            ->getQuery()
            ->getResult();
        
        foreach ($parentCategories as $category) {
            $item = [];
            
            $item['id'] = $category->getId();
            $item['text'] = $category->getItemTitle();
            
            if ($category->getParent() != null) {
                $item['parent'] = $category->getParent()->getId();
            } else {
                $item['parent'] = '#';
            }
            
            if ($category->getId() == $categoryId) {
                $item['state'] = ['selected' => true];
            }
            
            $categoriesArray[] = $item;
        }
        
        return new JsonResponse($categoriesArray);
    }
    
    public function loadCategoryFiltersAction(Request $request, $templateId = null) {
        $em = $this->getDoctrine()->getManager();
        $categoryId = (int)$request->request->get('selectedId');
        
        $category = $em->getRepository('LsAllegroBundle:Category')->findOneBy(['id' => $categoryId]);
        $filters = $category->getFilters();
        
        $template = $em->getRepository('LsAllegroBundle:FilterTemplate')->findOneBy(['id' => $templateId]);

        $filterForm = $this->createFilterForm($filters, $template);
        $filterForm->handleRequest($request);
        
        $response = array(
            'html' => iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsAllegroBundle:AdminTemplate:filtersForm.html.twig', array(
                'category' => $category,
                'filterForm' => $filterForm->createView(),
                'filters' => $filters
            ))->getContent())
        );
        
        return new JsonResponse($response);
    }
    
    /**
     * Adding new template.
     * @param Request $request
     * @return object
     */
    public function addTemplateAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = new FilterTemplate();
        
        $filterForm = $this->createFilterForm();
        $filterForm->handleRequest($request);
        
        if ($filterForm->isSubmitted() && $filterForm->isValid()) {
            $formData = $request->get("form");
            $formData = $this->prepareFormData($formData);
            
            $entity->setTitle($formData['title']);
            $entity->setResultSize((int)$formData['resultSize']);
            $entity->setMinResultSize((int)$formData['minResultSize']);
            $entity->setPriceDifference((int)$formData['priceDifference']);
            $entity->setMaxPriceDifference((int)$formData['maxPriceDifference']);
            $entity->setCategory($formData['category']);
            $entity->setIncludeKeywords($formData['includeKeywords']);
            $entity->setByYears($formData['byYears']);
            $entity->setExcludeKeywords($formData['excludeKeywords']);
            $entity->setFilters(json_encode($formData['filters']));
            $entity->setUpdatedAt(new \DateTime());

            $em->persist($entity);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Szablon został zapisany.');
            
            if ($filterForm->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_allegro_editfilter_template', array('id' => $entity->getId())));
            }
            if ($filterForm->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_allegro_templates'));
            }
            if ($filterForm->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_allegro_addfilter_template'));
            }
        }
        if ($filterForm->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Szablony filtrów', $this->get('router')->generate('ls_admin_allegro_templates'));
        $breadcrumbs->addItem('Dodaj szablon', $this->get('router')->generate('ls_admin_allegro_addfilter_template'));
        
        return $this->render('LsAllegroBundle:AdminTemplate:new.html.twig', array(
            'form' => $filterForm->createView()
        ));
    }
    
    public function editTemplateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('LsAllegroBundle:FilterTemplate')->find($id);
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Template entity.');
        }
        
        $filterForm = $this->createFilterForm();
        $filterForm->handleRequest($request);
        
        if ($filterForm->isSubmitted() && $filterForm->isValid()) {
            $formData = $request->get("form");
            $formData = $this->prepareFormData($formData);

            $entity->setTitle($formData['title']);
            $entity->setResultSize((int)$formData['resultSize']);
            $entity->setMinResultSize((int)$formData['minResultSize']);
            $entity->setPriceDifference((int)$formData['priceDifference']);
            $entity->setMaxPriceDifference((int)$formData['maxPriceDifference']);
            $entity->setCategory($formData['category']);
            $entity->setIncludeKeywords($formData['includeKeywords']);
            $entity->setByYears($formData['byYears']);
            $entity->setExcludeKeywords($formData['excludeKeywords']);
            $entity->setFilters(json_encode($formData['filters']));
            $entity->setUpdatedAt(new \DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Aktualiacja szablonu powiodła się.');
            
            if ($filterForm->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_allegro_editfilter_template', array('id' => $entity->getId())));
            }
            if ($filterForm->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_allegro_templates'));
            }
            if ($filterForm->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_allegro_addfilter_template'));
            }
        }
        if ($filterForm->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Szablony filtrów', $this->get('router')->generate('ls_admin_allegro_templates'));
        $breadcrumbs->addItem($entity->getTitle(), $this->get('router')->generate('ls_admin_allegro_editfilter_template'));
        
        return $this->render('LsAllegroBundle:AdminTemplate:edit.html.twig', array(
            'form' => $filterForm->createView(),
            'entity' => $entity
        ));
        
    }    
    
    private function prepareFormData($formData) {
        $em = $this->getDoctrine()->getManager();
        $preparedData = [];
        
        $defaultFields = ['submit', '_token'];
        
        foreach ($formData as $key => $data) {
            $fields = ['title', 'resultSize', 'minResultSize', 'priceDifference', 'includeKeywords', 'excludeKeywords', 'byYears', 'maxPriceDifference'];
            
            if (in_array($key, $fields)) {
                $preparedData[$key] = $data;
            } elseif ($key == 'category_id') {
                $category = $em->getRepository('LsAllegroBundle:Category')->findOneBy(['id' => $data]);
                $preparedData['category'] = $category;
            } elseif (!in_array($key, $defaultFields)) {
                $preparedData['filters'][$key] = $data;
            }
        }
        
        return $preparedData;
    }
    
    public function deleteTemplateAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsAllegroBundle:FilterTemplate')->find($id);

        if ($entity) {
            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Usunięcie szablonu zakończone sukcesem.');
        }

        return $this->redirectToRoute('ls_admin_allegro_templates');
    }
    
    public function batchAction(Request $request) {
        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Podstrony', $this->get('router')->generate('ls_admin_allegro_templates'));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_allegro_templates_batch'));

            return $this->render('LsAllegroBundle:AdminTemplate:batch.html.twig', array(
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_allegro_templates'));
        }
    }

    public function batchExecuteAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsAllegroBundle:FilterTemplate', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        $em->flush();
                    }

                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_allegro_templates'));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_allegro_templates'));
        }
    }
    
    private function createFilterForm($filters = null, $template = null)
    {
        $filtersData = [];
        $title = null;
        $includeKeywords = null;
        $excludeKeywords = null;
        $byYears = true;
        $resultSize = 30;
        $minResultSize = 2;
        $priceDifference = 10;
        $maxPriceDifference = 10;
        
        if ($template instanceof FilterTemplate) {
            $title = $template->getTitle();
            $byYears = $template->getByYears();
            $resultSize = $template->getResultSize();
            $minResultSize = $template->getMinResultSize();
            $priceDifference = $template->getPriceDifference();
            $includeKeywords = $template->getIncludeKeywords();
            $excludeKeywords = $template->getExcludeKeywords();
            $maxPriceDifference = $template->getMaxPriceDifference();
            $filtersData = json_decode($template->getFilters(), true);
        }
        
        $defaultData = array();
        $defaultOptions = ['allow_extra_fields' => true];
        
        $form = $this->createFormBuilder($defaultData, $defaultOptions);
        
        $form->add('title', null, array(
            'label' => 'Nazwa filtru',
            'data' => $title,
            'attr' => ['class' => 'form-control'],
            'mapped' => false,
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        
        $form->add('byYears', CheckboxType::class, array(
            'label' => 'Grupowanie wg roku produkcji',
            'data' => $byYears,
            'mapped' => false,
        ));
        
        $form->add('resultSize', IntegerType::class, array(
            'label' => 'Maksymalna ilość najtańszych przeszukiwanych ofert',
            'data' => $resultSize,
            'attr' => ['class' => 'form-control'],
            'attr' => [
                'class' => 'form-control',
                'min' => 1,
                'max' => 50,
            ],
            'mapped' => false,
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        
        $form->add('minResultSize', IntegerType::class, array(
            'label' => 'Minimalna ilość najtańszych przeszukiwanych ofert',
            'data' => $minResultSize,
            'attr' => ['class' => 'form-control'],
            'attr' => [
                'class' => 'form-control',
                'min' => 1,
                'max' => 50,
            ],
            'mapped' => false,
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        
        $form->add('priceDifference', IntegerType::class, array(
            'label' => 'Próg cenowy (w procentach)',
            'data' => $priceDifference,
            'attr' => [
                'class' => 'form-control',
                'min' => 1,
                'max' => 100,
            ],
            'mapped' => false,
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        
        $form->add('maxPriceDifference', IntegerType::class, array(
            'label' => 'Max możliwy próg cenowy',
            'data' => $maxPriceDifference,
            'attr' => [
                'class' => 'form-control',
                'min' => 1,
                'max' => 100,
            ],
            'mapped' => false,
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        
        $form->add('includeKeywords', null, array(
            'label' => 'Zawiera słowa kluczowe (oddzielone przecinkami)',
            'data' => $includeKeywords,
            'attr' => ['class' => 'form-control'],
            'mapped' => false,
        ));
        
        $form->add('excludeKeywords', null, array(
            'label' => 'Nie zawiera słowa kluczowe (oddzielone przecinkami)',
            'data' => $excludeKeywords,
            'attr' => ['class' => 'form-control'],
            'mapped' => false,
        ));
        
        $form->add('category_id', HiddenType::class, array(
            'attr' => ['style' => 'display:none;'],
            'label_attr' => ['style' => 'display:none;'],
            'mapped' => false,
        ));
                   
        if ($filters != null) {
            foreach ($filters as $filter) {
                if ($filter->getFilterControlType() == "textbox" && $filter->getTitle() != 'Kategoria') {
                    if ($filter->getFilterIsRange()) {                        
                        $form->add('start_'.$filter->getId(), null, array(
                            'label' => $filter->getTitle(),
                            'required' => false,
                            'mapped' => false,
                            'data' => (key_exists('start_'.$filter->getId(), $filtersData)) ? $filtersData['start_'.$filter->getId()] : "",
                            'attr' => [
                                'placeholder' => 'Od',
                                'data_range' => $filter->getTitle(),
                                'class' => 'form-control'
                            ]
                        ));
                        
                        $form->add('end_'.$filter->getId(), null, array(
                            'label' => false,
                            'required' => false,
                            'mapped' => false,
                            'data' => (key_exists('end_'.$filter->getId(), $filtersData)) ? $filtersData['end_'.$filter->getId()] : "",
                            'attr' => [
                                'placeholder' => 'Do',
                                'data_range' => $filter->getTitle(),
                                'class' => 'form-control'
                            ],
                            'label_attr' => [
                                'style' => 'display: none;'
                            ]
                        ));
                    } else {
                        $form->add($filter->getId(), null, array(
                            'label' => $filter->getTitle(),
                            'data' => (key_exists($filter->getId(), $filtersData)) ? $filtersData[$filter->getId()] : "",
                            'required' => false,
                            'mapped' => false,
                            'attr' => ['class' => 'form-control']
                        ));
                    }
                }
                
                if ($filter->getFilterControlType() == "combobox" && $filter->getTitle() != 'Dział') {
                    $multiple = false;
                    if ($filter->getTitle() == 'Województwo') {
                        $multiple = true;
                    }
                    
                    $values = (key_exists($filter->getId(), $filtersData)) ? $filter->getSelectedValues($filtersData[$filter->getId()], $multiple) : null;
                    
                    $form->add($filter->getId(), ChoiceType::class, array(
                        'label' => $filter->getTitle(),
                        'choices'  => $filter->getValues(),
                        'empty_data' => $values,
                        'multiple' => $multiple,  
                        'data' => $values,
                        'mapped' => false,
                        'placeholder' => '- brak -',
                        'choice_label' => function($value) {
                            return $value->getTitle();
                        },
                        'choice_value' => function ($entity = null) {
                            return $entity ? $entity->getId() : '';
                        },
                        'attr' => ['class' => 'form-control chosen-select']
                    ));
                }
                
                if ($filter->getFilterControlType() == "checkbox") {  
                    $values = (key_exists($filter->getId(), $filtersData)) ? $filter->getSelectedValues($filtersData[$filter->getId()], true) : null;
                    $form->add($filter->getId(), ChoiceType::class, array(
                        'label' => $filter->getTitle(),
                        'choices'  => $filter->getValues(),
                        'empty_data' => $values,
                        'data' => $values,
                        'multiple' => true,  
                        'expanded' => true,  
                        'mapped' => false,
                        'placeholder' => '- brak -',
                        'choice_label' => function($value) {
                            return $value->getTitle();
                        },
                        'choice_value' => function (FilterValue $entity = null) {
                            return $entity ? $entity->getId() : '';
                        },
                        'attr' => ['class' => 'checkbox-group']
                    ));
                }
            }
        }
        
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz', 'attr' => ['class' => 'btn btn-primary']));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę', 'attr' => ['class' => 'btn btn-default']));
        $form->add('submit_and_new', SubmitType::class, array('label' => 'Zapisz i dodaj następny', 'attr' => ['class' => 'btn btn-default']));
        
        $form = $form->getForm();

        return $form;
    }
    
    public function setLimitAction(Request $request) {
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }
}
