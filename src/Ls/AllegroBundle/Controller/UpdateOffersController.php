<?php

namespace Ls\AllegroBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ls\AllegroBundle\Utils\AllegroOffers;
use Ls\AllegroBundle\Utils\AllegroUpdater;
use Ls\AllegroBundle\Entity\Offers;
use Ls\AllegroBundle\Utils\OffersModel;
use Ls\AllegroBundle\Entity\FilterTemplate;
use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\Client;

class UpdateOffersController extends Controller {
    
    public function getAllTemplates() {
        $em = $this->getDoctrine()->getManager();
        
        $templates = $em->createQueryBuilder()
            ->select('e')
            ->from('LsAllegroBundle:FilterTemplate', 'e')
            ->getQuery()
            ->getResult();
       
        return $templates;
    }
    
    //remove old offers and get list of offers to check new offers
    public function removeOldOffersAndGetList($templateId, $categoryId, $newOffersIds) {
        $em = $this->getDoctrine()->getManager();
        $ids = [];
        
        $oldOffers = $em->createQueryBuilder()
            ->select('o')
            ->from('LsAllegroBundle:Offers', 'o')
            ->leftJoin('o.template', 't')
            ->leftJoin('o.category', 'c')
            ->where('t.id = :templateId')
            ->andWhere('c.id = :categoryId')
            ->setParameter('templateId', $templateId)
            ->setParameter('categoryId', $categoryId)
            ->getQuery()
            ->getResult();
            
        foreach ($oldOffers as $oldOffer) {
            if (!in_array($oldOffer->getItemId(), $newOffersIds)) {
                $em->remove($oldOffer);
            } else {
                $ids[] = $oldOffer->getItemId();
            }
        }
        
        $em->flush();
        
        return $ids;
    }
    
    public function updateOffersCommand($templateId, $categoryId) {
        $em = $this->getDoctrine()->getManager();
                
        $entity = $em->createQueryBuilder()
            ->select('e')
            ->from('LsAllegroBundle:FilterTemplate', 'e')
            ->where('e.id = :templateId')
            ->setParameter('templateId', $templateId)
            ->getQuery()
            ->getOneOrNullResult();
        
        $category = $em->createQueryBuilder()
            ->select('c')
            ->from('LsAllegroBundle:Category', 'c')
            ->where('c.id = :categoryId')
            ->setParameter('categoryId', $categoryId)
            ->getQuery()
            ->getOneOrNullResult();
        
        if ($entity != null && $category != null) {
            $offersInfo = null;
            $allegroOffers = new AllegroOffers($this->getDoctrine()->getManager(), $entity, $category);
            $offers = $allegroOffers->getOffers();  
            $offers = $this->checkOffersPrice($offers);
            $promotedOffers = $this->getPromotedOffers($entity, $offers, $entity->getPriceDifference());
            
            $oldOffersIds = $this->removeOldOffersAndGetList($templateId, $categoryId, $promotedOffers->getOffersAllegroIds());
            foreach ($promotedOffers->getOffers() as $year => $offersList) {
                foreach ($offersList as $offer) {
                    if (!in_array($offer->itemId, $oldOffersIds)) {
                        $newOffer = new Offers();
                        $newOffer->setItemId($offer->itemId);
                        $newOffer->setPrice($offer->priceInfo->item[0]->priceValue);
                        $newOffer->setTemplate($entity);
                        $newOffer->setTitle($offer->itemTitle);
                        $newOffer->setYear($year);
                        $newOffer->setCategory($category);

                        $em->persist($newOffer);
                    }
                }
            }

            $em->flush();
        }
                
        return "Updated";
    }
    
    public function getPromotedOffers(FilterTemplate $template, OffersModel $offersModel, $priceDifference = 10) {
        $promotedOffers = [];
        $offersInfo = [];
        $hasBigPriceDiff = false;
        
        if ($template->getIncludeKeywords() != null || $template->getExcludeKeywords() != null) {
            $offersInfo = $this->getOffersInfo($offersModel);
        }
        
        foreach ($offersModel->getOffers() as $year => $offers) { 
            if (count($offers) >= $template->getMinResultSize()) {
                $addedOffers = 0;
                foreach ($offers as $key => $offer) {
                    
                    $checkIncludeKeywords = $this->checkIncludeKeywords($template, $offer, $offersInfo);
                    $checkExcludeKeywords = $this->checkExcludeKeywords($template, $offer, $offersInfo);
                    
                    $currentPrice = $offer->priceInfo->item[0]->priceValue;
                    $priceDifferencePercents = ($priceDifference/100);
                    $maxPriceDifferencePercents = ($template->getMaxPriceDifference()/100);
                    
                    //check if first offer has big price diff
                    if ($key == 0) {
                        if (key_exists($key + 1, $offers)) {
                            $nextPrice = $offers[$key + 1]->priceInfo->item[0]->priceValue;
                            $hasBigPriceDiff = $nextPrice > ($currentPrice + ($currentPrice * $maxPriceDifferencePercents));
                            
                            if ($hasBigPriceDiff) {
                                continue;
                            }
                        }
                    }
                    
                    if (key_exists($key - 1, $offers)) {
                        $prevPrice = $offers[$key - 1]->priceInfo->item[0]->priceValue;
                        $hasPromotionalPrice = $currentPrice < ($prevPrice + ($prevPrice * $priceDifferencePercents));
                    } else {
                        $nextPrice = $offers[$key + 1]->priceInfo->item[0]->priceValue;
                        $hasPromotionalPrice = $currentPrice < ($nextPrice + ($nextPrice * $priceDifferencePercents));
                    }

                    if ($checkIncludeKeywords) {
                        $addedOffers++;
                        $promotedOffers[$year][] = $offer;
                        $offersModel->addOfferAllegroId($offer->itemId);
                    } else {
                        if ($hasPromotionalPrice) {
                            if ($checkExcludeKeywords) {
                                $addedOffers++;
                                $promotedOffers[$year][] = $offer;
                                $offersModel->addOfferAllegroId($offer->itemId);
                            }
                        } else {
                            break;
                        }
                    }
                    
                }
                
                if ($hasBigPriceDiff) {
                    array_shift($offers);
                }
                
                if ($addedOffers == count($offers)) {
                    if (key_exists($year, $promotedOffers)) {
                        $promotedOffers[$year] = [];
                    }
                }
            }
        }
        
        $offersModel->setOffers($promotedOffers);
        
        return $offersModel;
    }
    
    //check if price netto and add VAT 23%
    private function checkOffersPrice(OffersModel $offersList) {
        foreach ($offersList->getOffers() as $year => $offers) {
            foreach ($offers as $offer) {
                $offerUrl = "http://allegro.pl/show_item.php?item=".$offer->itemId;
                
                try {
                    $client = new Client();
                    $request = $client->get($offerUrl);
                    $crawler = new Crawler(null, $offerUrl);
                    $crawler->addHtmlContent($request->getBody()->getContents());
                    $priceInfoHtml = $crawler->filter(".offer-price > .offer-price__details"); //price info in otomoto.pl

                    if (($priceInfoHtml->count()) > 0) {
                        $priceInfo = $priceInfoHtml->text();

                        if (preg_match("/(Cena|cena) (Netto|netto)/", $priceInfo)) {
                            $offer->priceInfo->item[0]->priceValue = $offer->priceInfo->item[0]->priceValue + ($offer->priceInfo->item[0]->priceValue * 0.23);
                        }
                    }
                } catch (\Exception $e) {
                    continue; //skip if can`t connect with otomoto
                }
            }
        }
        
        return $offersList;
    }
    
    //return true if has keywords
    private function checkIncludeKeywords(FilterTemplate $template, $offer, $offersInfo) {        
        if ($template->getIncludeKeywords() != null) {
            $keywords = explode(',', $template->getIncludeKeywords());
            foreach ($keywords as $keyword) {
                $keyword = trim($keyword);
                if (strpos($offer->itemTitle, $keyword) !== false) {
                    return true;
                } else {
                    $offerDescription = "";
                    foreach ($offersInfo as $offerInfo) {
                        if ($offerInfo->{'item-info'}->{'it-id'} == $offer->itemId) {
                            $offerDescription = $offerInfo->{'item-info'}->{'it-description'};
                        }
                    }
                    
                    if (strpos($offerDescription, $keyword) !== false) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    // return false if has keywords
    private function checkExcludeKeywords(FilterTemplate $template, $offer, $offersInfo) {        
        if ($template->getExcludeKeywords() != null) {
            $keywords = explode(',', $template->getExcludeKeywords());
            foreach ($keywords as $keyword) {
                $keyword = trim($keyword);
                if (strpos($offer->itemTitle, $keyword) !== false) {
                    return false;
                } else {
                    $offerDescription = "";
                    foreach ($offersInfo as $offerInfo) {
                        if ($offerInfo->{'item-info'}->{'it-id'} == $offer->itemId) {
                            $offerDescription = $offerInfo->{'item-info'}->{'it-description'};
                        }
                    }
                    
                    if (strpos($offerDescription, $keyword) !== false) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    
    private function getOffersInfo(OffersModel $offersModel) {
        $allegroUpdater = new AllegroUpdater();
        $itemsIdArray = [];

        foreach ($offersModel->getOffers() as $offersList) {
            foreach ($offersList as $offer) {
                if (count($itemsIdArray) < 25) { //allegro limit 25
                    $itemsIdArray[] = $offer->itemId;
                }
            }
        }

        return $allegroUpdater->getOfferItemsInfo($itemsIdArray);
    }
}
