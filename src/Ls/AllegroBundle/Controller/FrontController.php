<?php

namespace Ls\AllegroBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Ls\AllegroBundle\Utils\OffersRssProvider;

class FrontController extends Controller {
    
    public function allOffersRssAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        
        $lastOffer = $em->createQueryBuilder()
            ->select('t')
            ->from('LsAllegroBundle:Offers', 't')
            ->orderBy('t.created_at', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        
        if ($lastOffer) {
            $lastModified = $lastOffer->getCreatedAt();
        } else {
            $lastModified = new \DateTime();
        }
        
        $offers = $em->createQueryBuilder()
            ->select('t')
            ->from('LsAllegroBundle:Offers', 't')
            ->getQuery()
            ->getResult();
                
        $rss = new OffersRssProvider($this->container);
        
        return $rss->getRss($request, null, $offers, $lastModified);
    }
    
    public function offersRssAction(Request $request, $templateId) {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository("LsAllegroBundle:FilterTemplate")->findOneBy(['id' => $templateId]);
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FilterTemplate entity.');
        }
        
        $lastOffer = $em->createQueryBuilder()
            ->select('t')
            ->from('LsAllegroBundle:Offers', 't')
            ->where('t.template = :template')
            ->setParameter('template', $entity)
            ->orderBy('t.created_at', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        
        if ($lastOffer) {
            $lastModified = $lastOffer->getCreatedAt();
        } else {
            $lastModified = new \DateTime();
        }
        
        $offers = $em->createQueryBuilder()
            ->select('t')
            ->from('LsAllegroBundle:Offers', 't')
            ->where('t.template = :template')
            ->setParameter('template', $entity)
            ->getQuery()
            ->getResult();
                
        $rss = new OffersRssProvider($this->container);
        
        return $rss->getRss($request, $entity, $offers, $lastModified);
    }
    
    
    public function offersByYearsRssAction(Request $request, $templateId, $year) {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository("LsAllegroBundle:FilterTemplate")->findOneBy(['id' => $templateId]);
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FilterTemplate entity.');
        }
        
        $lastOffer = $em->createQueryBuilder()
            ->select('t')
            ->from('LsAllegroBundle:Offers', 't')
            ->where('t.template = :template')
            ->andWhere('t.year = :year')
            ->setParameter('template', $entity)
            ->setParameter('year', $year)
            ->orderBy('t.created_at', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        
        if ($lastOffer) {
            $lastModified = $lastOffer->getCreatedAt();
        } else {
            $lastModified = new \DateTime();
        }
        
        $offers = $em->createQueryBuilder()
            ->select('t')
            ->from('LsAllegroBundle:Offers', 't')
            ->where('t.template = :template')
            ->andWhere('t.year = :year')
            ->setParameter('template', $entity)
            ->setParameter('year', $year)
            ->getQuery()
            ->getResult();
        
        $rss = new OffersRssProvider($this->container);
        return $rss->getRss($request, $entity, $offers, $lastModified);
    }
    
    public function categoryOffersRssAction(Request $request, $templateId, $categoryId) {
        $em = $this->getDoctrine()->getManager();
        
        $template = $em->getRepository("LsAllegroBundle:FilterTemplate")->findOneBy(['id' => $templateId]);
        
        if (!$template) {
            throw $this->createNotFoundException('Unable to find FilterTemplate entity.');
        }
        
        $category = $em->getRepository("LsAllegroBundle:Category")->findOneBy(['id' => $categoryId]);
        
        if (!$category) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }
        
        $lastCategories = [];
        
        if (count($category->getChildren()) > 0) {
            $lastCategories = $this->getLastChilds($category->getChildren(), []);
        } else {
            $lastCategories[] = $category;
        }
        
        $lastOffer = $em->createQueryBuilder()
            ->select('t')
            ->from('LsAllegroBundle:Offers', 't')
            ->where('t.template = :template')
            ->andWhere('t.category IN (:categories)')
            ->setParameter('template', $template)
            ->setParameter('categories', $lastCategories)
            ->orderBy('t.created_at', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        
        if ($lastOffer) {
            $lastModified = $lastOffer->getCreatedAt();
        } else {
            $lastModified = new \DateTime();
        }
        
        $offers = $em->createQueryBuilder()
            ->select('t')
            ->from('LsAllegroBundle:Offers', 't')
            ->where('t.template = :template')
            ->andWhere('t.category IN (:categories)')
            ->setParameter('template', $template)
            ->setParameter('categories', $lastCategories)
            ->getQuery()
            ->getResult();
                
        $rss = new OffersRssProvider($this->container);
        
        return $rss->getRss($request, $template, $offers, $lastModified);
    }
    
    
    public function categoryOffersByYearsRssAction(Request $request, $templateId, $categoryId, $year) {
        $em = $this->getDoctrine()->getManager();
        
        $template = $em->getRepository("LsAllegroBundle:FilterTemplate")->findOneBy(['id' => $templateId]);
        
        if (!$template) {
            throw $this->createNotFoundException('Unable to find FilterTemplate entity.');
        }
        
        $category = $em->getRepository("LsAllegroBundle:Category")->findOneBy(['id' => $categoryId]);
        
        if (!$category) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }
        
        $lastCategories = [];
        
        if (count($category->getChildren()) > 0) {
            $lastCategories = $this->getLastChilds($category->getChildren(), []);
        } else {
            $lastCategories[] = $category;
        }
        
        $lastOffer = $em->createQueryBuilder()
            ->select('t')
            ->from('LsAllegroBundle:Offers', 't')
            ->where('t.template = :template')
            ->andWhere('t.category IN (:categories)')
            ->andWhere('t.year = :year')
            ->setParameter('template', $template)
            ->setParameter('year', $year)
            ->setParameter('categories', $lastCategories)
            ->orderBy('t.created_at', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        
        if ($lastOffer) {
            $lastModified = $lastOffer->getCreatedAt();
        } else {
            $lastModified = new \DateTime();
        }
        
        $offers = $em->createQueryBuilder()
            ->select('t')
            ->from('LsAllegroBundle:Offers', 't')
            ->where('t.template = :template')
            ->andWhere('t.category IN (:categories)')
            ->andWhere('t.year = :year')
            ->setParameter('template', $template)
            ->setParameter('year', $year)
            ->setParameter('categories', $lastCategories)
            ->getQuery()
            ->getResult();
        
        $rss = new OffersRssProvider($this->container);
        return $rss->getRss($request, $template, $offers, $lastModified);
    }
}
