<?php

namespace Ls\AllegroBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ls\AllegroBundle\Entity\Category;
use Ls\AllegroBundle\Entity\Filter;
use Ls\AllegroBundle\Entity\FilterValue;
use Ls\AllegroBundle\Utils\AllegroUpdater;

class AdminCategoriesController extends Controller {
    private $pager_limit_name = 'admin_allegro_pager_limit';
    private $count = 0;
    
    private $parseCategories = ['Motoryzacja', 'Maszyny', 'Samochody'];
    
    public function categoriesListAction(Request $request, $parentId) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');
        $parent = null;
        
        if ($parentId > 0) {
            $parent = $em->getRepository('LsAllegroBundle:Category')->find($parentId);
            if (!$parent) {
                throw $this->createNotFoundException('Unable to find Category parent entity.');
            }
        }
        
        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $qb = $em->createQueryBuilder();
        
        $qb->select('e');
        $qb->from('LsAllegroBundle:Category', 'e');
        $qb->leftJoin('e.parent', 'p');
        
        if ($parentId > 0) {
            $qb->andWhere('p.id = :parentId');
            $qb->setParameter('parentId', $parentId);
        } else {
            $qb->andWhere('p.id is NULL');
        }
        
        $query = $qb->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.id',
                'defaultSortDirection' => 'asc',
            )
        );
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_allegro_categories', ['parentId' => $parentId]));
        }
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Lista kategorii', $this->get('router')->generate('ls_admin_allegro_categories'));
        
        if ($parent != null) {
            foreach ($parent->getAllParents() as $item) {
                $breadcrumbs->addItem($item->__toString(), $this->get('router')->generate('ls_admin_allegro_categories', ['parentId' => $item->getId()]));
            }
            $breadcrumbs->addItem($parent->__toString(), $this->get('router')->generate('ls_admin_allegro_categories', ['parentId' => $parent->getId()]));
        }
        
        return  $this->render('LsAllegroBundle:Admin:categoriesList.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
            'parentId' => $parentId
        ));
    }
    
    public function filtersListAction(Request $request, $categoryId) {
        $em = $this->getDoctrine()->getManager();
        
        $category = $em->getRepository('LsAllegroBundle:Category')->findOneBy(['id' => $categoryId]);
        
        if (!$category) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }
        
        $entities = $em->createQueryBuilder()
            ->select('e', 'v')
            ->from('LsAllegroBundle:Filter', 'e')
            ->leftJoin('e.categories', 'c')
            ->leftJoin('e.values', 'v')
            ->where('c.id = :categoryId')
            ->setParameter('categoryId', $categoryId)
            ->getQuery()
            ->getResult();
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Lista kategorii', $this->get('router')->generate('ls_admin_allegro_categories'));
        
        if ($category != null) {
            foreach ($category->getAllParents() as $item) {
                $breadcrumbs->addItem($item->__toString(), $this->get('router')->generate('ls_admin_allegro_categories', ['parentId' => $item->getId()]));
            }
            $breadcrumbs->addItem($category->__toString(), $this->get('router')->generate('ls_admin_allegro_categories', ['parentId' => $category->getId()]));
        }
        
        $breadcrumbs->addItem('Lista filtrów', $this->get('router')->generate('ls_admin_allegro_categories_filters', ['categoryId' => $categoryId]));
        
        $parentId = 0;
        
        if ($category->getParent() != null) {
            $parentId = $category->getParent()->getId();
        }
        
        return  $this->render('LsAllegroBundle:Admin:filtersList.html.twig', array(
            'entities' => $entities,
            'categoryId' => $categoryId,
            'parentId' => $parentId
        ));
    }
    
    public function getAllCategories() {
        $em = $this->getDoctrine()->getManager();
        
        $categories = $em->getRepository('LsAllegroBundle:Category')->findAll();
        
        return $categories;
    }
    
    public function updateFiltersAction($categoryId) {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 14400);
        
        $em = $this->getDoctrine()->getManager();
        $updater = new AllegroUpdater();
        
        $category = $em->getRepository('LsAllegroBundle:Category')->findOneBy(['id' => $categoryId]);
        
        $filters = $updater->updateFiltersList($category->getCategoryAllegroId()); 

        foreach ($filters as $filter) {
            $existingFilter = $em->getRepository('LsAllegroBundle:Filter')->findOneBy(['title' => $filter->filterName,'filterId' => $filter->filterId]);

            if (!$existingFilter) {
                $newFilter = new Filter();
                $newFilter->addCategory($category);
                $newFilter->setTitle($filter->filterName);
                $newFilter->setFilterType($filter->filterType);
                $newFilter->setFilterControlType($filter->filterControlType);
                $newFilter->setFilterIsRange($filter->filterIsRange);
                $newFilter->setFilterId($filter->filterId);

                $em->persist($newFilter);
                $em->flush();

                if (isset($filter->filterValues)) {
                    $this->addFilterValues($newFilter, $filter->filterValues);
                }

            } else {
                $existingFilter->addCategory($category);
                $em->persist($existingFilter);
                $em->flush();
            }

        }
      
        //$this->get('session')->getFlashBag()->set('success', 'Aktualizacja zakończona sukcesem.');
        return new Response("OK");
    }
    
    public function addFilterValues(Filter $filter, $filterValues) {
        $em = $this->getDoctrine()->getManager();

        foreach ($filterValues as $filterValuesItem) {
            foreach ($filterValuesItem as $filterValue) {
                $existingFilterValue = $em->getRepository('LsAllegroBundle:FilterValue')->findOneBy(['title' => $filterValue->filterValueName, 'filterValueId' => $filterValue->filterValueId]);
                if (!$existingFilterValue) {
                    $newFilterValue = new FilterValue();
                    $newFilterValue->addFilter($filter);
                    $newFilterValue->setTitle($filterValue->filterValueName);
                    $newFilterValue->setFilterValueId($filterValue->filterValueId);

                    $em->persist($newFilterValue);
                    $em->flush();
                } else {
                    $existingFilterValue->addFilter($filter);
                    $em->persist($existingFilterValue);
                    $em->flush();
                }
            }
        }
    }
    
    public function updateCategoriesAction() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 7200);
        
        $em = $this->getDoctrine()->getManager();
        $updater = new AllegroUpdater();
        $this->count = 0;
        $categories = $updater->getCategoryTree();
        $categoriesTree = $this->buildTree($categories);
        
        foreach ($categoriesTree as $Item) {
            if (in_array($Item->catName, $this->parseCategories)) {
                $existingCategory = $em->getRepository('LsAllegroBundle:Category')->findOneBy(array('categoryAllegroId' => $Item->catId));

                if (! $existingCategory) {
                    $this->count++;
                    $Category = new Category();
                    $Category->setItemTitle($Item->catName);
                    $Category->setParentAllegroId($Item->catParent);
                    $Category->setCategoryAllegroId($Item->catId);

                    $em->persist($Category);
                    $em->flush();

                    if (count($Item->childs)) {
                        $this->addFirstChilds($Category, $Item);
                    }
                } else {
                    if (count($Item->childs)) {
                        $this->addFirstChilds($existingCategory, $Item);
                    }
                }   
            }
        }
        
        $this->get('session')->getFlashBag()->set('success', 'Zaktualizowano ' . $this->count . ' kategorii.');
        
        return $this->redirectToRoute('ls_admin_allegro_categories');
    }    
    
    public function addFirstChilds(Category $parentCategoryEntity, $parentCategory) {
        $em = $this->getDoctrine()->getManager();
        
        foreach ($parentCategory->childs as $child) {
            if (in_array($child->catName, $this->parseCategories)) {
                $existingCategory = $em->getRepository('LsAllegroBundle:Category')->findOneBy(array('categoryAllegroId' => $child->catId));

                if (! $existingCategory) {
                    $this->count++;
                    $Category = new Category();
                    $Category->setItemTitle($child->catName);
                    $Category->setParentAllegroId($child->catParent);
                    $Category->setCategoryAllegroId($child->catId);
                    $Category->setParent($parentCategoryEntity);

                    $em->persist($Category);
                    $em->flush();

                    if (count($child->childs)) {
                        $this->addChilds($Category, $child);
                    }
                } else {
                    if (count($child->childs)) {
                        $this->addChilds($existingCategory, $child);
                    }
                }
            }
        }
    }
    
    public function addChilds(Category $parentCategoryEntity, $parentCategory) {
        $em = $this->getDoctrine()->getManager();
        
        foreach ($parentCategory->childs as $child) {
            $existingCategory = $em->getRepository('LsAllegroBundle:Category')->findOneBy(array('categoryAllegroId' => $child->catId));
            
            if (! $existingCategory) {
                $this->count++;
                $Category = new Category();
                $Category->setItemTitle($child->catName);
                $Category->setParentAllegroId($child->catParent);
                $Category->setCategoryAllegroId($child->catId);
                $Category->setParent($parentCategoryEntity);

                $em->persist($Category);
                $em->flush();
            
                if (count($child->childs)) {
                    $this->addChilds($Category, $child);
                }
            } else {
                if (count($child->childs)) {
                    $this->addChilds($existingCategory, $child);
                }
            }
        }
    }
    
    public function buildTree($items) {
        $childs = array();
        foreach($items as $item) {
            $childs[$item->catParent][] = $item;
        }
        foreach($items as $item) {
            if (isset($childs[$item->catId])) {
                $item->childs = $childs[$item->catId];
            } else {
                $item->childs = [];
            }
        }
        
        return $childs[0];
    }
    
    public function setLimitAction(Request $request) {
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }
}
