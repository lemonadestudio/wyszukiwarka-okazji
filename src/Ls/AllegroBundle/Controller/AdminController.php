<?php

namespace Ls\AllegroBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller {
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        
        $entities = $em->getRepository("LsAllegroBundle:FilterTemplate")->findAll();
        
        foreach ($entities as $entity) {
            $lastUpdate = $this->getLastUpdateTime($entity);
            $entity->setOffersUpdatedAt($lastUpdate);
        }
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Oferty', $this->get('router')->generate('ls_admin_allegro'));

        return $this->render('LsAllegroBundle:Admin:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    
    private function getLastUpdateTime($template) {
        $em = $this->getDoctrine()->getManager();
        
        $lastOffer = $em->createQueryBuilder()
            ->select('t')
            ->from('LsAllegroBundle:Offers', 't')
            ->where('t.template = :template')
            ->setParameter('template', $template)
            ->orderBy('t.created_at', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        
        if (!$lastOffer) {
            return null;
        }
        
        return $lastOffer->getCreatedAt();
    }
    
    public function offersAction(Request $request, $templateId) {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository("LsAllegroBundle:FilterTemplate")->findOneBy(['id' => $templateId]);
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FilterTemplate entity.');
        }
        
        $offers = $em->createQueryBuilder()
            ->select('t')
            ->from('LsAllegroBundle:Offers', 't')
            ->where('t.template = :template')
            ->setParameter('template', $entity)
            ->orderBy('t.price', 'asc')
            ->getQuery()
            ->getResult();
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Oferty', $this->get('router')->generate('ls_admin_allegro'));
        $breadcrumbs->addItem($entity->getTitle(), $this->get('router')->generate('ls_admin_allegro'));

        return $this->render('LsAllegroBundle:Admin:offersAll.html.twig', array(
            'template' => $entity,
            'offers' => $offers,
        ));
    }
    
    public function yearsAction(Request $request, $templateId) {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository("LsAllegroBundle:FilterTemplate")->findOneBy(['id' => $templateId]);
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FilterTemplate entity.');
        }
        
        $years = $em->createQueryBuilder()
            ->select('count(t) as offersCount, t.year as year')
            ->from('LsAllegroBundle:Offers', 't')
            ->where('t.template = :template')
            ->setParameter('template', $entity)
            ->orderBy('t.price', 'asc')
            ->groupBy('t.year')
            ->getQuery()
            ->getResult();
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Oferty', $this->get('router')->generate('ls_admin_allegro'));
        $breadcrumbs->addItem($entity->getTitle(), $this->get('router')->generate('ls_admin_allegro'));

        return $this->render('LsAllegroBundle:Admin:years.html.twig', array(
            'template' => $entity,
            'years' => $years
        ));
    }
    
    public function offersByYearsAction(Request $request, $templateId, $year) {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository("LsAllegroBundle:FilterTemplate")->findOneBy(['id' => $templateId]);
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FilterTemplate entity.');
        }
        
        $offers = $em->createQueryBuilder()
            ->select('t')
            ->from('LsAllegroBundle:Offers', 't')
            ->where('t.template = :template')
            ->andWhere('t.year = :year')
            ->setParameter('template', $entity)
            ->setParameter('year', $year)
            ->orderBy('t.price', 'asc')
            ->getQuery()
            ->getResult();
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Oferty', $this->get('router')->generate('ls_admin_allegro'));
        $breadcrumbs->addItem($entity->getTitle(), $this->get('router')->generate('ls_admin_allegro_years', ['templateId' => $templateId]));
        $breadcrumbs->addItem($year, $this->get('router')->generate('ls_admin_allegro'));

        return $this->render('LsAllegroBundle:Admin:offers.html.twig', array(
            'template' => $entity,
            'year' => $year,
            'offers' => $offers,
        ));
    }
    
    public function categoriesAction(Request $request, $templateId, $categoryId) {
        $em = $this->getDoctrine()->getManager();
        
        $template = $em->getRepository("LsAllegroBundle:FilterTemplate")->findOneBy(['id' => $templateId]);
        
        if (!$template) {
            throw $this->createNotFoundException('Unable to find FilterTemplate entity.');
        }
        
        $category = $em->getRepository("LsAllegroBundle:Category")->findOneBy(['id' => $categoryId]);
        
        if (!$category) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }
        
        $offers = null;
        $subcategories = $category->getChildren();
        
        if (count($subcategories) > 0) {
            foreach ($subcategories as $subcategory) {
                if (count($subcategory->getChildren()) > 0) {
                    $lastChilds = $this->getLastChilds($subcategory->getChildren(), []);
                    foreach ($lastChilds as $lastCategory) {
                        $offers = $this->getOffersByCategory($template, $lastCategory);
                        foreach ($offers as $offer) {
                            $subcategory->addPromotedOffer($offer);
                        }
                    }
                } else {
                    $offers = $this->getOffersByCategory($template, $subcategory);
                    foreach ($offers as $offer) {
                        $subcategory->addPromotedOffer($offer);
                    }
                }
            }
        }
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem("Szablon " . $template->getTitle(), $this->get('router')->generate('ls_admin_allegro'));        
        $breadcrumbs->addItem($category->getItemTitle(), $this->get('router')->generate('ls_admin_allegro_categories_list', ['templateId' => $templateId, 'categoryId' => $categoryId]));
        
        return $this->render('LsAllegroBundle:Admin:subcategoriesList.html.twig', array(
            'template' => $template,
            'category' => $category,
            'subcategories' => $subcategories
        ));
    }
    
    
    public function categoryYearsAction(Request $request, $templateId, $categoryId) {
        $em = $this->getDoctrine()->getManager();
        
        $template = $em->getRepository("LsAllegroBundle:FilterTemplate")->findOneBy(['id' => $templateId]);
        
        if (!$template) {
            throw $this->createNotFoundException('Unable to find FilterTemplate entity.');
        }
        
        $category = $em->getRepository("LsAllegroBundle:Category")->findOneBy(['id' => $categoryId]);
        
        if (!$category) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }
        
        $lastCategories = [];
        
        if (count($category->getChildren()) > 0) {
            $lastCategories = $this->getLastChilds($category->getChildren(), []);
        } else {
            $lastCategories[] = $category;
        }
        
        $years = $em->createQueryBuilder()
            ->select('count(t) as offersCount, t.year as year')
            ->from('LsAllegroBundle:Offers', 't')
            ->where('t.template = :template')
            ->andWhere('t.category IN (:categories)')
            ->setParameter('template', $template)
            ->setParameter('categories', $lastCategories)
            ->orderBy('t.price', 'asc')
            ->groupBy('t.year')
            ->getQuery()
            ->getResult();
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem("Szablon " . $template->getTitle(), $this->get('router')->generate('ls_admin_allegro'));
        
        if ($category->getParent() != null) {
            $breadcrumbs->addItem($category->getParent()->getItemTitle(), $this->get('router')->generate('ls_admin_allegro_categories_list', ['templateId' => $templateId, 'categoryId' => $category->getParent()->getId()]));
        }
        
        $breadcrumbs->addItem($category->getItemTitle(), $this->get('router')->generate('ls_admin_allegro_categories_list_years', ['templateId' => $templateId, 'categoryId' => $category->getId()]));

        return $this->render('LsAllegroBundle:Admin:categoryYears.html.twig', array(
            'template' => $template,
            'category' => $category,
            'years' => $years
        ));
    }
    
    
    public function categoryOffersByYearsAction(Request $request, $templateId, $categoryId, $year) {
        $em = $this->getDoctrine()->getManager();
        
        $template = $em->getRepository("LsAllegroBundle:FilterTemplate")->findOneBy(['id' => $templateId]);
        
        if (!$template) {
            throw $this->createNotFoundException('Unable to find FilterTemplate entity.');
        }
        
        $category = $em->getRepository("LsAllegroBundle:Category")->findOneBy(['id' => $categoryId]);
        
        if (!$category) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }
        
        $lastCategories = [];
        
        if (count($category->getChildren()) > 0) {
            $lastCategories = $this->getLastChilds($category->getChildren(), []);
        } else {
            $lastCategories[] = $category;
        }
        
        $offers = $em->createQueryBuilder()
            ->select('t')
            ->from('LsAllegroBundle:Offers', 't')
            ->where('t.template = :template')
            ->andWhere('t.category IN (:categories)')
            ->andWhere('t.year = :year')
            ->setParameter('template', $template)
            ->setParameter('year', $year)
            ->setParameter('categories', $lastCategories)
            ->orderBy('t.price', 'asc')
            ->getQuery()
            ->getResult();
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem("Szablon " . $template->getTitle(), $this->get('router')->generate('ls_admin_allegro'));
        
        if ($category->getParent() != null) {
            $breadcrumbs->addItem($category->getParent()->getItemTitle(), $this->get('router')->generate('ls_admin_allegro_categories_list', ['templateId' => $templateId, 'categoryId' => $category->getParent()->getId()]));
        }
        
        $breadcrumbs->addItem($category->getItemTitle(), $this->get('router')->generate('ls_admin_allegro_categories_list_years', ['templateId' => $templateId, 'categoryId' => $category->getId()]));
        $breadcrumbs->addItem($year, $this->get('router')->generate('ls_admin_allegro'));

        return $this->render('LsAllegroBundle:Admin:categoryOffersYears.html.twig', array(
            'template' => $template,
            'category' => $category,
            'year' => $year,
            'offers' => $offers,
        ));
    }
    
    
    public function categoryOffersAction(Request $request, $templateId, $categoryId) {
        $em = $this->getDoctrine()->getManager();
        
        $template = $em->getRepository("LsAllegroBundle:FilterTemplate")->findOneBy(['id' => $templateId]);
        
        if (!$template) {
            throw $this->createNotFoundException('Unable to find FilterTemplate entity.');
        }
        
        $category = $em->getRepository("LsAllegroBundle:Category")->findOneBy(['id' => $categoryId]);
        
        if (!$category) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }
        
        $lastCategories = [];
        
        if (count($category->getChildren()) > 0) {
            $lastCategories = $this->getLastChilds($category->getChildren(), []);
        } else {
            $lastCategories[] = $category;
        }
        
        $offers = $em->createQueryBuilder()
            ->select('t')
            ->from('LsAllegroBundle:Offers', 't')
            ->where('t.template = :template')
            ->andWhere('t.category IN (:categories)')
            ->setParameter('template', $template)
            ->setParameter('categories', $lastCategories)
            ->orderBy('t.price', 'asc')
            ->getQuery()
            ->getResult();
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem("Szablon " . $template->getTitle(), $this->get('router')->generate('ls_admin_allegro'));
        
        if ($category->getParent() != null) {
            $breadcrumbs->addItem($category->getParent()->getItemTitle(), $this->get('router')->generate('ls_admin_allegro_categories_list', ['templateId' => $templateId, 'categoryId' => $category->getParent()->getId()]));
        }
        
        $breadcrumbs->addItem($category->getItemTitle(), $this->get('router')->generate('ls_admin_allegro_categories_list', ['templateId' => $templateId, 'categoryId' => $categoryId]));

        return $this->render('LsAllegroBundle:Admin:categoryOffersAll.html.twig', array(
            'template' => $template,
            'category' => $category,
            'offers' => $offers,
        ));
    }
    
    private function getOffersByCategory($template, $category) {
        $em = $this->getDoctrine()->getManager();
        
        $offers = $em->createQueryBuilder()
            ->select('t')
            ->from('LsAllegroBundle:Offers', 't')
            ->where('t.category = :category')
            ->andWhere('t.template = :template')
            ->setParameter('category', $category)
            ->setParameter('template', $template)
            ->orderBy('t.price', 'asc')
            ->getQuery()
            ->getResult();
        
        return $offers;
    }
    
    private function getLastChilds($childs, $lastChilds) {
        
        foreach ($childs as $children) {
            if (count($children->getChildren()) > 0) {
                $lastChilds = $this->getLastChilds($children->getChildren(), $lastChilds);
            } else {
                $lastChilds[] = $children;
            }
        }

        return $lastChilds;
    }
}
