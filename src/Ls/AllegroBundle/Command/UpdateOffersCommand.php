<?php
namespace Ls\AllegroBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Process\Process;

class UpdateOffersCommand extends ContainerAwareCommand {
    
    private $logFile = __DIR__ . "/logs.txt";
    private $processList = []; // queue of simultaneous connections
    private $processesLimit = 40; // limit for simultaneous connections
    
    protected function configure()
    {
        // once run this command in CLI as background process
        // e.g. nohup php bin/console allegro:update:offers >/dev/null 2>&1 &
        $this
            ->setName('allegro:update:offers')
            ->setDescription('Aktualizacja ofert');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $updater = $this->getContainer()->get('ls_allegro.update_controller'); 
        
        while (true) {
        	$templates = $updater->getAllTemplates();    

        	$startedAt = "Started at ".date('d.m.Y H:i:s')."\n";
            echo $startedAt;
            file_put_contents($this->logFile, $startedAt, FILE_APPEND);

            $startTime = new \DateTime();

            foreach ($templates as $template) {
                $mainCategory = $template->getCategory();
                if (count($mainCategory->getChildren()) > 0) {
                    $lastCategories = $this->getLastChilds($mainCategory->getChildren(), []);

                    foreach ($lastCategories as $category) {
                        //windows command
                        //$command = "C:\wamp64\bin\php\php5.6.19\php.exe G:\projects\wyszukiwarka-okazji\bin\console allegro:update:template ".$template->getId()." ".$category->getId();

                        //linux command
                        $command = "nohup /usr/bin/php /var/www/allegro/bin/console allegro:update:template ".$template->getId()." ".$category->getId();

                        $this->processQueueCheck();
                        $process = new Process($command);
                        $process->start();
                        $this->processList[] = $process;
                    }
                } else {
                    //windows command
                    //$command = "C:\wamp64\bin\php\php5.6.19\php.exe G:\projects\wyszukiwarka-okazji\bin\console allegro:update:template ".$template->getId()." ".$mainCategory->getId();

                    //linux command
                    $command = "nohup /usr/bin/php /var/www/allegro/bin/console allegro:update:template ".$template->getId()." ".$mainCategory->getId();

                    $this->processQueueCheck();
                    $process = new Process($command);
                    $process->start();
                    $this->processList[] = $process;
                }
            }

            //waitimg for all processes then end script
            foreach ($this->processList as $processItem) {
                $processItem->wait(function ($type, $buffer) {
                    if (Process::ERR === $type) {
                        echo 'ERR > '.$buffer;
                        file_put_contents($this->logFile, 'ERR > '.$buffer."\n", FILE_APPEND);
                    }
                });
            }

            $finishedAt = "Finished at ".date('d.m.Y H:i:s')."\n";
            echo $finishedAt;
            file_put_contents($this->logFile, $finishedAt, FILE_APPEND);
            file_put_contents($this->logFile, "---------------------------", FILE_APPEND);


            $endTime = new \DateTime();
            $delay = $endTime->getTimestamp() - $startTime->getTimestamp(); // because processor time limit
            
            sleep(ceil($delay * 5));
        }
        
        $output->writeln("Updater end");
    }
    
    private function getLastChilds($childs, $lastChilds) {
        
        foreach ($childs as $children) {
            if (count($children->getChildren()) > 0) {
                $lastChilds = $this->getLastChilds($children->getChildren(), $lastChilds);
            } else {
                $lastChilds[] = $children;
            }
        }

        return $lastChilds;
    }
    
    private function processQueueCheck() {
        if (count($this->processList) > $this->processesLimit) {
            foreach ($this->processList as $key => $processItem) {
                if (count($this->processList) > $this->processesLimit) {
                    $processItem->wait(function ($type, $buffer) {
                        if (Process::ERR === $type) {
                            echo 'ERR > '.$buffer;
                            file_put_contents($this->logFile, 'ERR > '.$buffer."\n", FILE_APPEND);
                        }
                    });
                    // $processItem->wait() -- wait for process end then remove process from queue
                    unset($this->processList[$key]);
                }
            }
        }
    }
}
    