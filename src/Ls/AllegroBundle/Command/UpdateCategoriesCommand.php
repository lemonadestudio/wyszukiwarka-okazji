<?php
namespace Ls\AllegroBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;

class UpdateCategoriesCommand extends ContainerAwareCommand {
    
    protected function configure()
    {
        $this
            ->setName('allegro:update:categories')
            ->setDescription('Aktualizacja kategorii');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $updater = $this->getContainer()->get('ls_allegro.categories_controller'); 
        $response = $updater->updateCategoriesAction();   
        
        $output->writeln($response);
    }
}
    