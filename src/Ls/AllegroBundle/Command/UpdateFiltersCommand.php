<?php
namespace Ls\AllegroBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;

class UpdateFiltersCommand extends ContainerAwareCommand {
    
    protected function configure()
    {
        $this
            ->setName('allegro:update:filterbycategory')
            ->setDescription('Aktualizacja filtrów wg kategorii')
            ->addArgument(
                'categoryId',
                InputArgument::REQUIRED,
                'Category Id'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $categoryId = $input->getArgument('categoryId');
        
        $updater = $this->getContainer()->get('ls_allegro.categories_controller'); 
        $updater->updateFiltersAction($categoryId);
        
        $output->writeln("Updater end");
    }
}
    