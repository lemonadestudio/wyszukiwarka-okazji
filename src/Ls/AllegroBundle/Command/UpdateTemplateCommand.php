<?php
namespace Ls\AllegroBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;

class UpdateTemplateCommand extends ContainerAwareCommand {
    
    protected function configure()
    {
        $this
            ->setName('allegro:update:template')
            ->setDescription('Aktualizacja ofert')
            ->addArgument(
                'templateId',
                InputArgument::REQUIRED,
                'Templade Id'
            )
            ->addArgument(
                'categoryId',
                InputArgument::REQUIRED,
                'Category Id'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $templateId = $input->getArgument('templateId');
        $categoryId = $input->getArgument('categoryId');
        
        $updater = $this->getContainer()->get('ls_allegro.update_controller'); 
        $response = $updater->updateOffersCommand($templateId, $categoryId);   
        
        $output->writeln($response);
    }
}
    