<?php
namespace Ls\AllegroBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Process\Process;

class UpdateCategoriesFiltersCommand extends ContainerAwareCommand {
    
    private $logFile = __DIR__ . "/logs.txt";
    private $processList = []; // queue of simultaneous connections
    private $processesLimit = 40; // limit for simultaneous connections
    
    protected function configure()
    {
        $this
            ->setName('allegro:update:filters')
            ->setDescription('Aktualizacja filtrów');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $updater = $this->getContainer()->get('ls_allegro.categories_controller'); 
        
        while (true) {
            $categories = $updater->getAllCategories(); 
 
            $startTime = new \DateTime();
            
            foreach ($categories as $category) {
                //windows command
                //$command = "C:\wamp64\bin\php\php5.6.19\php.exe G:\projects\wyszukiwarka-okazji\bin\console allegro:update:template ".$template->getId()." ".$mainCategory->getId();

                //linux command
                $command = "/usr/bin/php /var/www/allegro/bin/console allegro:update:filterbycategory ".$category->getId();

                $this->processQueueCheck();
                $process = new Process($command);
                $process->start();
                $this->processList[] = $process;
            }

            //waitimg for all processes then end script
            foreach ($this->processList as $processItem) {
                $processItem->wait(function ($type, $buffer) {
                    if (Process::ERR === $type) {
                        echo 'ERR > '.$buffer;
                        file_put_contents($this->logFile, 'ERR > '.$buffer."\n", FILE_APPEND);
                    }
                });
            }
            
            $endTime = new \DateTime();
            $delay = $endTime->getTimestamp() - $startTime->getTimestamp(); // because processor time limit
            
            sleep(ceil($delay * 2));
        }

        $output->writeln("Updater end");
    }
    
    private function processQueueCheck() {
        if (count($this->processList) > $this->processesLimit) {
            foreach ($this->processList as $key => $processItem) {
                if (count($this->processList) > $this->processesLimit) {
                    $processItem->wait(function ($type, $buffer) {
                        if (Process::ERR === $type) {
                            $logFile = __DIR__ . "/logs.txt";
                            file_put_contents($this->logFile, 'ERR > '.$buffer."\n", FILE_APPEND);
                            echo 'ERR > '.$buffer;
                        }
                    });
                    // $processItem->wait() -- wait for process end then remove process from queue
                    unset($this->processList[$key]);
                }
            }
        }
    }
}
    