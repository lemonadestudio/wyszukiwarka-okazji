<?php

namespace Ls\AllegroBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Ls\AllegroBundle\Entity\Category;
use Ls\AllegroBundle\Entity\Offers;

/**
 * Filter
 * @ORM\Table(name="category")
 * @ORM\Entity
 */
class Category
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string  
     */
    private $itemTitle;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $categoryAllegroId;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $parentAllegroId;
    
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;
    
    /**
     * @ORM\OneToMany(
     *   targetEntity="Category",
     *   mappedBy="parent",
     *   cascade={"persist"},
     *   orphanRemoval=true
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $children;

    /**
     * @ORM\ManyToOne(
     *   targetEntity="Category",
     *   inversedBy="children"
     * )
     * @var Subcategory
     */
    private $parent;
        
    /**
     * @ORM\ManyToMany(targetEntity="Filter", mappedBy="categories")
     * @ORM\JoinTable(name="category_filters",
     *      joinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="filter_id", referencedColumnName="id")}
     *      )
     */
    private $filters;
    
    /**
     * @ORM\OneToMany(targetEntity="Offers", mappedBy="category")
     */
    private $offers;
    
    private $promotedOffersList = [];

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->seo_generate = true;
        $this->created_at = new \DateTime();
        $this->children = new ArrayCollection();
        $this->filters = new ArrayCollection();
        $this->offers = new ArrayCollection();
    }
    
    public function __toString() {
        return $this->itemTitle;
    }
    
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set itemTitle
     *
     * @param string $itemTitle
     *
     * @return Category
     */
    public function setItemTitle($itemTitle)
    {
        $this->itemTitle = $itemTitle;

        return $this;
    }

    /**
     * Get itemTitle
     *
     * @return string
     */
    public function getItemTitle()
    {
        return $this->itemTitle;
    }

    /**
     * Set categoryAllegroId
     *
     * @param string $categoryAllegroId
     *
     * @return Category
     */
    public function setCategoryAllegroId($categoryAllegroId)
    {
        $this->categoryAllegroId = $categoryAllegroId;

        return $this;
    }

    /**
     * Get categoryAllegroId
     *
     * @return string
     */
    public function getCategoryAllegroId()
    {
        return $this->categoryAllegroId;
    }

    /**
     * Set parentAllegroId
     *
     * @param string $parentAllegroId
     *
     * @return Category
     */
    public function setParentAllegroId($parentAllegroId)
    {
        $this->parentAllegroId = $parentAllegroId;

        return $this;
    }

    /**
     * Get parentAllegroId
     *
     * @return string
     */
    public function getParentAllegroId()
    {
        return $this->parentAllegroId;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Category
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    /**
     * Add children
     *
     * @param \Ls\OfferBundle\Entity\Subcategory $children
     * @return MenuItem
     */
    public function addChildren(Category $children) {
        $this->children->add($children);

        return $this;
    }

    /**
     * Remove children
     *
     * @param Category $children
     */
    public function removeChildren(Category $children) {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param Category $parent
     * @return MenuItem
     */
    public function setParent(Category $parent = null) {
        $this->parent = $parent;

        return $this;
    }
    
    
    /**
     * Get parent
     *
     * @return \Ls\OfferBundle\Entity\Subcategory
     */
    public function getParent() {
        return $this->parent;
    }
    
    /**
     * Add filter
     *
     * @param Filters $filter
     */
    public function addFilter(Filter $filter) {
        if (!$this->filters->contains($filter)) {
            $this->filters[] = $filter;
        }
        return $this;
    }
    
    /**
     * Remove filter
     *
     * @param Filters $filter
     */
    public function removeFilter(Filter $filter) {
        $this->filters->removeElement($filter);
    }

    
    public function getFilters() {
        return $this->filters;
    }
    
    public function getAllParents() {
        $parents = [];
        $parents = $this->getPrevParent($this, $parents);
        
        $parents = array_reverse($parents);
        
        return $parents;
    }
    
    public function getPrevParent($object, $parents) {
        if ($object->getParent() != null) {
            $parents[] = $object->getParent();
            $parents = $this->getPrevParent($object->getParent(), $parents);
        }
        
        return $parents;
    }
    
    /**
     * Add offer
     *
     * @param Offers $offer
     */
    public function addOffer(Offers $offer) {
        if (!$this->offers->contains($offer)) {
            $this->offers->add($offer);
        }
        return $this;
    }
    
    /**
     * Remove offer
     *
     * @param Offers $offer
     */
    public function removeOffer(Offers $offer) {
        $this->offers->removeElement($offer);
    }
    
    public function getOffers() {
        return $this->offers;
    }

    public function addPromotedOffer($promotedOffer) {
        if (!in_array($promotedOffer, $this->promotedOffersList)) {
            $this->promotedOffersList[] = $promotedOffer;
        }
        return $this;
    }

    public function getPromotedOffersList() {
        return $this->promotedOffersList;
    }
}
