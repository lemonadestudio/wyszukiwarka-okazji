<?php

namespace Ls\AllegroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ls\AllegroBundle\Entity\FilterTemplate;
use Ls\AllegroBundle\Entity\Category;

/**
 * @ORM\Table(name="offers")
 * @ORM\Entity
 */
class Offers
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $title;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $price;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $year;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $itemId;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;    
    
    /**
     * @ORM\ManyToOne(targetEntity="FilterTemplate", inversedBy="offers")
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id")
     */
    private $template;
    
    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="offers")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();
    }
    
    public function __toString() {
        return $this->title;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return FilterTemplate
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return FilterTemplate
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set itemId
     *
     * @param string $itemId
     *
     * @return FilterTemplate
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * Get itemId
     *
     * @return string
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Set year
     *
     * @param string $year
     *
     * @return FilterTemplate
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set template
     *
     * @param FilterTemplate $template
     *
     * @return Offer
     */
    public function setTemplate(FilterTemplate $template)
    {
        $this->template = $template;

        return $this;
    }
    
    public function getTemplate() {
        return $this->template;
    }   

    /**
     * Set category
     *
     * @param Category $category
     *
     * @return Offer
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;

        return $this;
    }
    
    public function getCategory() {
        return $this->category;
    }   

    /**
     * Set created_at
     *
     * @param $created_at
     *
     * @return Offer
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }
    
    public function getCreatedAt() {
        return $this->created_at;
    }   
}
