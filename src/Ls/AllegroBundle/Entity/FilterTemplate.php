<?php

namespace Ls\AllegroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ls\AllegroBundle\Entity\Category;
use Doctrine\Common\Collections\ArrayCollection;
use Ls\AllegroBundle\Entity\Offers;

/**
 * @ORM\Table(name="filter_template")
 * @ORM\Entity
 */
class FilterTemplate
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $title;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $offersChecksum;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $includeKeywords;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $excludeKeywords;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var string
     */
    private $byYears;
    
    /**
     * max możliwy próg cenowy, czyli taki powyżej którego oferta nie jest uznawana za okazyjną (chodzi o odrzucenie ofert gdzie różnica ceny wynosi np. 60%)
     * @ORM\Column(type="integer", length=3, nullable=true)
     * @var string
     */
    private $maxPriceDifference;
    
    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     * @var string
     */
    private $resultSize;
    
    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     * @var string
     */
    private $minResultSize;
    
    /**
     * @ORM\Column(type="integer", length=3, nullable=true)
     * @var string
     */
    private $priceDifference;
    
    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;
    
    /**
     * @ORM\Column(type="json_array", nullable=true)
     * @var string
     */
    private $filters;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;    

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;    

    /**
     * @ORM\OneToMany(targetEntity="Offers", mappedBy="template", cascade={"persist"}, orphanRemoval=true)
     */
    private $offers;
    
    private $offers_updated_at;  
    private $hasYear;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
        $this->priceDifference = 10;
        $this->resultSize = 30;
        $this->minResultSize = 2;
        $this->offers = new ArrayCollection();
        $this->hasYear = false;
        $this->byYears = true;
        $this->maxPriceDifference = 60;
    }
    
    public function __toString() {
        return $this->itemTitle;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return FilterTemplate
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set byYears
     *
     * @param string $byYears
     *
     * @return FilterTemplate
     */
    public function setByYears($byYears)
    {
        $this->byYears = $byYears;

        return $this;
    }

    /**
     * Get byYears
     *
     * @return string
     */
    public function getByYears()
    {
        return $this->byYears;
    }

    /**
     * Set priceDifference
     *
     * @param string $priceDifference
     *
     * @return FilterTemplate
     */
    public function setPriceDifference($priceDifference)
    {
        $this->priceDifference = $priceDifference;

        return $this;
    }

    /**
     * Get priceDifference
     *
     * @return string
     */
    public function getPriceDifference()
    {
        return $this->priceDifference;
    }

    /**
     * Set maxPriceDifference
     *
     * @param string $maxPriceDifference
     *
     * @return FilterTemplate
     */
    public function setMaxPriceDifference($maxPriceDifference)
    {
        $this->maxPriceDifference = $maxPriceDifference;

        return $this;
    }

    /**
     * Get maxPriceDifference
     *
     * @return string
     */
    public function getMaxPriceDifference()
    {
        return $this->maxPriceDifference;
    }

    /**
     * Set created_at
     *
     * @param string $created_at
     *
     * @return FilterTemplate
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param string $updated_at
     *
     * @return FilterTemplate
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set offers_updated_at
     *
     * @param string $offers_updated_at
     *
     * @return FilterTemplate
     */
    public function setOffersUpdatedAt($offers_updated_at)
    {
        $this->offers_updated_at = $offers_updated_at;

        return $this;
    }

    /**
     * Get offers_updated_at
     *
     * @return string
     */
    public function getOffersUpdatedAt()
    {
        return $this->offers_updated_at;
    }

    /**
     * Set resultSize
     *
     * @param string $resultSize
     *
     * @return FilterTemplate
     */
    public function setResultSize($resultSize)
    {
        $this->resultSize = $resultSize;

        return $this;
    }

    /**
     * Get resultSize
     *
     * @return string
     */
    public function getResultSize()
    {
        return $this->resultSize;
    }

    /**
     * Set minResultSize
     *
     * @param string $minResultSize
     *
     * @return FilterTemplate
     */
    public function setMinResultSize($minResultSize)
    {
        $this->minResultSize = $minResultSize;

        return $this;
    }

    /**
     * Get minResultSize
     *
     * @return string
     */
    public function getMinResultSize()
    {
        return $this->minResultSize;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return FilterTemplate
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set filters
     *
     * @param string $filters
     *
     * @return FilterTemplate
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;

        return $this;
    }

    /**
     * Get filters
     *
     * @return string
     */
    public function getFilters()
    {
        return $this->filters;
    }
    
    /**
     * Set offersChecksum
     *
     * @param string $offersChecksum
     *
     * @return FilterTemplate
     */
    public function setOffersChecksum($offersChecksum)
    {
        $this->offersChecksum = $offersChecksum;

        return $this;
    }

    /**
     * Get offersChecksum
     *
     * @return string
     */
    public function getOffersChecksum()
    {
        return $this->offersChecksum;
    }
    
    /**
     * Add offer
     *
     * @param Offers $offer
     */
    public function addOffer(Offers $offer) {
        if (!$this->offers->contains($offer)) {
            $this->offers->add($offer);
        }
        return $this;
    }
    
    /**
     * Remove offer
     *
     * @param Offers $offer
     */
    public function removeOffer(Offers $offer) {
        $this->offers->removeElement($offer);
    }
    
    public function getOffers() {
        return $this->offers;
    }
    
    /**
     * Set hasYear
     *
     * @param string $hasYear
     *
     * @return FilterTemplate
     */
    public function setHasYear($hasYear)
    {
        $this->hasYear = $hasYear;

        return $this;
    }

    /**
     * Get hasYear
     *
     * @return string
     */
    public function getHasYear()
    {
        return $this->hasYear;
    }
    
    /**
     * Set includeKeywords
     *
     * @param string $includeKeywords
     *
     * @return FilterTemplate
     */
    public function setIncludeKeywords($includeKeywords)
    {
        $this->includeKeywords = $includeKeywords;

        return $this;
    }

    /**
     * Get includeKeywords
     *
     * @return string
     */
    public function getIncludeKeywords()
    {
        return $this->includeKeywords;
    }
    
    /**
     * Set excludeKeywords
     *
     * @param string $excludeKeywords
     *
     * @return FilterTemplate
     */
    public function setExcludeKeywords($excludeKeywords)
    {
        $this->excludeKeywords = $excludeKeywords;

        return $this;
    }

    /**
     * Get excludeKeywords
     *
     * @return string
     */
    public function getExcludeKeywords()
    {
        return $this->excludeKeywords;
    }
}
