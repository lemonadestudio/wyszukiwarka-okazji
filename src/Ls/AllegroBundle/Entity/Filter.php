<?php

namespace Ls\AllegroBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Ls\AllegroBundle\Entity\Category;
use Ls\AllegroBundle\Entity\FilterValue;

/**
 * Filter
 * @ORM\Table(name="filter")
 * @ORM\Entity
 */
class Filter
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string  
     */
    private $title;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $filterId;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $filterType;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $filterControlType;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var string
     */
    private $filterIsRange;
        
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;
    
    /**
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="filters")
     * @ORM\JoinTable(name="category_filters",
     *      joinColumns={@ORM\JoinColumn(name="filter_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *      )
     */
    private $categories;

    /**
     * @ORM\ManyToMany(targetEntity="FilterValue", mappedBy="filters")
     * @ORM\JoinTable(name="filters_values",
     *      joinColumns={@ORM\JoinColumn(name="filter_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="value_id", referencedColumnName="id")}
     *      )
     */
    private $values;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();;
        $this->categories = new ArrayCollection();
        $this->values = new ArrayCollection();
    }
    
    public function __toString() {
        return $this->title;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CategoryFilter
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set filterId
     *
     * @param string $filterId
     *
     * @return CategoryFilter
     */
    public function setFilterId($filterId)
    {
        $this->filterId = $filterId;

        return $this;
    }

    /**
     * Get filterId
     *
     * @return string
     */
    public function getFilterId()
    {
        return $this->filterId;
    }

    /**
     * Set filterType
     *
     * @param string $filterType
     *
     * @return CategoryFilter
     */
    public function setFilterType($filterType)
    {
        $this->filterType = $filterType;

        return $this;
    }

    /**
     * Get filterType
     *
     * @return string
     */
    public function getFilterType()
    {
        return $this->filterType;
    }

    /**
     * Set filterControlType
     *
     * @param string $filterControlType
     *
     * @return CategoryFilter
     */
    public function setFilterControlType($filterControlType)
    {
        $this->filterControlType = $filterControlType;

        return $this;
    }

    /**
     * Get filterControlType
     *
     * @return string
     */
    public function getFilterControlType()
    {
        return $this->filterControlType;
    }

    /**
     * Set filterIsRange
     *
     * @param boolean $filterIsRange
     *
     * @return CategoryFilter
     */
    public function setFilterIsRange($filterIsRange)
    {
        $this->filterIsRange = $filterIsRange;

        return $this;
    }

    /**
     * Get filterIsRange
     *
     * @return boolean
     */
    public function getFilterIsRange()
    {
        return $this->filterIsRange;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CategoryFilter
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
     
    /**
     * Add category
     *
     * @param Category $category
     */
    public function addCategory(Category $category) {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }
        return $this;
    }
    
    /**
     * Remove category
     *
     * @param Category $category
     */
    public function removeCategory(Category $category) {
        $this->categories->removeElement($category);
    }

    
    public function getCategories() {
        return $this->categories;
    }
     
    /**
     * Add value
     *
     * @param FilterValue $value
     */
    public function addValue(FilterValue $value) {
        if (!$this->values->contains($value)) {
            $this->values->add($value);
        }
        return $this;
    }
    
    /**
     * Remove value
     *
     * @param FilterValue $value
     */
    public function removeValue(FilterValue $value) {
        $this->values->removeElement($value);
    }
    
    public function getValues() {
        return $this->values;
    }
    
    public function getSelectedValues($selectedData, $multiple) {
        $selectedValues = [];
        
        foreach ($this->values as $value) {
            if (is_array($selectedData)) {
                if (in_array($value->getId(), $selectedData)) {
                    if ($multiple) {
                        $selectedValues[] = $value;
                    } else {
                        return $value;
                    }
                }
            }
        }
        
        return $selectedValues;
    }
}
