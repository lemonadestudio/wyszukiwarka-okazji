<?php

namespace Ls\AllegroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ls\AllegroBundle\Entity\Filter;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Filter Values
 * @ORM\Table(name="filter_value")
 * @ORM\Entity
 */
class FilterValue
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string  
     */
    private $title;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $filterValueId;
        
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;
    
    /**
     * @ORM\ManyToMany(targetEntity="Filter", inversedBy="values")
     * @ORM\JoinTable(name="filters_values",
     *      joinColumns={@ORM\JoinColumn(name="value_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="filter_id", referencedColumnName="id")}
     *      )
     */
    private $filters;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->filters = new ArrayCollection();
    }

    public function __toString() {
        return $this->title;
    }
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return FilterValue
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set filterValueId
     *
     * @param string $filterValueId
     *
     * @return FilterValue
     */
    public function setFilterValueId($filterValueId)
    {
        $this->filterValueId = $filterValueId;

        return $this;
    }

    /**
     * Get filterValueId
     *
     * @return string
     */
    public function getFilterValueId()
    {
        return $this->filterValueId;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return FilterValue
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    /**
     * Add filter
     *
     * @param Filters $filter
     */
    public function addFilter(Filter $filter) {
        if (!$this->filters->contains($filter)) {
            $this->filters->add($filter);
        }
        return $this;
    }
    
    /**
     * Remove filter
     *
     * @param Filters $filter
     */
    public function removeFilter(Filter $filter) {
        $this->filters->removeElement($filter);
    }

    
    public function getFilters() {
        return $this->filters;
    }
}
