<?php

namespace Ls\CoreBundle\Controller;

use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminDashboard;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller {
    protected $containerBuilder;

    public function indexAction() {
        return $this->render('LsCoreBundle:Default:index.html.twig', array());
    }

    public function adminAction() {
        $blocks = $this->container->getParameter('ls_core.admin.dashboard');
        $dashboard = new AdminDashboard();

        foreach($blocks as $block) {
            $parent = new AdminBlock($block['label']);
            $dashboard->addBlock($parent);
            foreach($block['items'] as $item) {
                $service = $this->container->get($item);
                $service->addToDashboard($parent);
            }
        }

        return $this->render('LsCoreBundle:Default:admin.html.twig', array(
            'dashboard' => $dashboard
        ));
    }

    public function KCFinderAction(Request $request)
    {
        $upload_dir = $this->get('kernel')->getRootDir() . '/../web/upload/pliki';

        $_SESSION['KCFINDER']['disabled'] = false;
        $_SESSION['KCFINDER']['uploadDir'] = $upload_dir;

        $getParameters = $request->query->all();

        
        return new RedirectResponse(
            $request->getBasePath() . 
            '/bundles/lscore/common/kcfinder-3.20/browse.php?' . 
            http_build_query($getParameters));
    }
}
